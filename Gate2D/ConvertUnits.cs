﻿using Microsoft.Xna.Framework;

namespace Gate2D
{
    internal class ConvertUnits
    {
        private static float _pixelsToWorldUnits = 24f;
        private static float _worldUnitsToPixels = 1f/_pixelsToWorldUnits;

        public static void SetPixelsPerWorldUnit(float pixels)
        {
            _pixelsToWorldUnits = pixels;
            _worldUnitsToPixels = 1f/pixels;
        }

        public static float ToDisplayUnits(float value)
        {
            return value*_pixelsToWorldUnits;
        }

        public static Vector2 ToDisplayUnits(Vector2 value)
        {
            return value*_pixelsToWorldUnits;
        }

        public static float ToWorldUnits(float value)
        {
            return value*_worldUnitsToPixels;
        }

        public static Vector2 ToWorldUnits(Vector2 value)
        {
            return value*_worldUnitsToPixels;
        }
    }
}