using System;
using FarseerPhysics.Controllers;
using FarseerPhysics.Dynamics;
using Gate2D.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Gate2D
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class GateGame : Game
    {
        private const float TimeStep = 1.0f/60.0f;

        private Border _border;

        private Ball _ball;
        private Box _box;
        private Texture2D _boxTex;
        private double _elapsed;

        private Portal _entry;
        private Portal _exit;
        private GraphicsDeviceManager _graphics;
        private Texture2D _portalTex;
        private SpriteBatch _spriteBatch;

        private World _world;

        public GateGame()
        {
            _graphics = new GraphicsDeviceManager(this)
                            {
                                PreferredBackBufferWidth = 1024,
                                PreferredBackBufferHeight = 600,
                                SynchronizeWithVerticalRetrace = true
                            };

            IsMouseVisible = true;
            IsFixedTimeStep = true;

            Window.AllowUserResizing = true;

            Content.RootDirectory = @"..\..\Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            _world = new World(new Vector2(0f, 20f));


            _entry = new Portal(_world, new Vector2(7, 20), 0f, 2f);
            _exit = new Portal(_world, new Vector2(27, 20), MathHelper.Pi, 2f);
            //_exit = new Portal(_world, new Vector2(27, 20), 2 * MathHelper.Pi / 3f, 2f);

            _entry.Target = _exit;
            _exit.Target = _entry;

            _box = new Box(_world, new Vector2(7, 2), 2f, 2f, 0f);
            _ball = new Ball(_world, new Vector2(15, 2), 1f);

            _border = new Border(_world, ConvertUnits.ToWorldUnits(GraphicsDevice.Viewport.Width),
                                 ConvertUnits.ToWorldUnits(GraphicsDevice.Viewport.Height));

            //var limitvelo = new VelocityLimitController(0.3f, 0);
            //limitvelo.EnabledOnGroup();
            //_world.AddController(limitvelo);
            //limitvelo.AddBody(_box.Body);

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _boxTex = Content.Load<Texture2D>("box");
            _portalTex = Content.Load<Texture2D>("portal");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();

            _elapsed += gameTime.ElapsedGameTime.TotalSeconds;

            while (_elapsed >= TimeStep)
            {
                _world.Step(TimeStep);
                _elapsed -= TimeStep;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied); // AlphaBlend not working in monogame/png?

            _spriteBatch.Draw(_boxTex, Vector2.Zero, null, new Color(0f, 0f, 0f, 0.25f), 0f,
                              Vector2.Zero,
                              ConvertUnits.ToDisplayUnits(_border.Extends)*
                              new Vector2(1.0f/_boxTex.Width, 1.0f/_boxTex.Height),
                              SpriteEffects.None, 0.0f);

            _spriteBatch.Draw(_portalTex, ConvertUnits.ToDisplayUnits(_entry.Body.Position), null, Color.White,
                              _entry.Body.Rotation,
                              new Vector2(_portalTex.Width/2f, _portalTex.Height/2f),
                              Vector2.One,
                              SpriteEffects.None, 0.0f);

            _spriteBatch.Draw(_portalTex, ConvertUnits.ToDisplayUnits(_exit.Body.Position), null, Color.White,
                              _exit.Body.Rotation,
                              new Vector2(_portalTex.Width/2f, _portalTex.Height/2f),
                              Vector2.One,
                              SpriteEffects.None, 0.0f);

            _spriteBatch.Draw(_boxTex, ConvertUnits.ToDisplayUnits(_box.Body.Position), null, Color.GreenYellow,
                              _box.Body.Rotation,
                              new Vector2(_boxTex.Width / 2f, _boxTex.Height / 2f),
                              ConvertUnits.ToDisplayUnits(_box.Extends) *
                              new Vector2(1.0f / _boxTex.Width, 1.0f / _boxTex.Height),
                              SpriteEffects.None, 0.0f);

            _spriteBatch.Draw(_boxTex, ConvertUnits.ToDisplayUnits(_ball.Body.Position), null, Color.PaleVioletRed,
                              _ball.Body.Rotation,
                              new Vector2(_boxTex.Width / 2f, _boxTex.Height / 2f),
                              ConvertUnits.ToDisplayUnits(2f) *
                              new Vector2(1.0f / _boxTex.Width, 1.0f / _boxTex.Height),
                              SpriteEffects.None, 0.0f);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}