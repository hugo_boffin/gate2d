﻿using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;

namespace Gate2D.Objects
{
    internal class Border
    {
        private readonly Body _body;

        private readonly Vector2 _extends;

        private readonly Vector2 _position;

        public Border(World world, float width, float height)
        {
            var vertices = new Vertices(4)
                               {
                                   new Vector2(0, 0),
                                   new Vector2(width, 0),
                                   new Vector2(width, height),
                                   new Vector2(0, height)
                               };
            _body = BodyFactory.CreateLoopShape(world, vertices);
            _body.CollisionCategories = Category.All;
            _body.CollidesWith = Category.All;

            _position = Vector2.Zero;
            _extends = new Vector2(width, height);
        }

        public Body Body
        {
            get { return _body; }
        }

        public Vector2 Extends
        {
            get { return _extends; }
        }

        public Vector2 Position
        {
            get { return _position; }
        }
    }
}