﻿using FarseerPhysics.Controllers;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;

namespace Gate2D.Objects
{
    internal class Box
    {
        private readonly Body _body;

        private readonly Vector2 _extends;

        public Box(World world, Vector2 position, float width, float height, float angle)
        {
            _body = BodyFactory.CreateRectangle(world, width, height, 5f);
            _body.SetTransform(position, angle);
            _body.BodyType = BodyType.Dynamic;
            _body.IsBullet = true;
            _body.Restitution = 1f;
            _body.LinearDamping = 0f;
            _body.AngularDamping = 0f;
            _body.Friction = 0f;
            //_body.ApplyForce(new Vector2(0, 1000));
            _body.ApplyAngularImpulse(10f);
            _body.ApplyTorque(1f);
            _extends = new Vector2(width, height);
        }

        public Body Body
        {
            get { return _body; }
        }

        public Vector2 Extends
        {
            get { return _extends; }
        }
    }
}