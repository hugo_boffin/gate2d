using System;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;

namespace Gate2D.Objects
{
    internal class Portal
    {
        private readonly Body _body;
        //private Vector2 _normal;

        public Portal(World world, Vector2 position, float angle, float width)
        {
            _body = BodyFactory.CreateEdge(world, new Vector2(width/-2f, 0), new Vector2(width/2f, 0));
            Body.IsSensor = true;
            Body.SetTransform(position, angle);
            Body.OnCollision += body_OnCollision;
            Target = null;

            //_normal = new Vector2((float) Math.Sin(angle), (float) Math.Cos(angle));
            //_normal.Normalize();
            //Console.WriteLine(_normal);
        }

        public Body Body
        {
            get { return _body; }
        }

        public Portal Target { get; set; }

        private bool body_OnCollision(Fixture fixtureA, Fixture fixtureB,
                                      Contact contact)
        {
            if (Target == null)
                return false;

            var traveler = fixtureB.Body;

            //var incDir = traveler.GetLinearVelocityFromWorldPoint(_body.Position);
            //incDir.Normalize();
            //float dot = Vector2.Dot(incDir, _normal);
            //Console.WriteLine(dot);

            var deltaPos = Target.Body.Position - Body.Position;
            var deltaAngle = Target.Body.Rotation - Body.Rotation;

            var teleportPos = traveler.Position + deltaPos;
            var teleportAngle = traveler.Rotation + deltaAngle;

            var rotation = Matrix.CreateRotationZ(deltaAngle);
            var newLinVelo = Vector2.Transform(traveler.LinearVelocity, rotation);

            //TODO: Angular Velocity & Torque, bzw. Fixed Rotation beim Player
            //Console.WriteLine(traveler.LinearVelocity);
            traveler.SetTransformIgnoreContacts(ref teleportPos, teleportAngle);
            //traveler.ResetDynamics();
            //traveler.ApplyLinearImpulse(newLinVelo*traveler.Mass);
            traveler.LinearVelocity = newLinVelo;
            Console.WriteLine(traveler.LinearVelocity);

            return false;
        }
    }
}