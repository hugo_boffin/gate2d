﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;

namespace Gate2D.Objects
{
    class Ball
    {
        private readonly Body _body;

        public Ball(World world, Vector2 position, float radius)
        {
            _body = BodyFactory.CreateCircle(world, radius, 1f);
            _body.BodyType = BodyType.Dynamic;
            _body.Restitution = 1f;
        }

        public Body Body
        {
            get { return _body; }
        }
    }
}
